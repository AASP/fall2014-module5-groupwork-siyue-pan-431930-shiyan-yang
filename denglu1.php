<?php
	header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);	
	$user = htmlentities(@$_POST['username']);
	$pwd_guess = htmlentities(@$_POST['password']);
	// Use a prepared statement
	require 'database.php';
	$stmt = $mysqli->prepare("SELECT password FROM users WHERE user_name=?");
	// Bind the parameter
	$stmt->bind_param('s', $user);
	$stmt->execute();
	// Bind the results
	$stmt->bind_result($pwd_hash);
	$stmt->fetch();
 
	// Compare the submitted password to the actual password hash
	if(crypt($pwd_guess, $pwd_hash)==$pwd_hash){
		session_start();
		$_SESSION['username'] = $user;
		$_SESSION['token'] = substr(md5(rand()), 0, 10);
	
		echo json_encode(array(
		"success" => true,
		"username" => htmlentities($user),
		"session" => htmlentities($_SESSION['token'])
		));
		exit;
	}else{
		echo json_encode(array(
		"success" => false,
		"message" => htmlentities("Incorrect Username or Password")
		));
		exit;
	}
?>