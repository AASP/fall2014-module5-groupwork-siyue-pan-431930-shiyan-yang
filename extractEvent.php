<?php
header("Content-Type: application/json"); 
ini_set("session.cookie_httponly", 1); 
session_start();
$date = htmlentities(@$_POST['date']);
$username = htmlentities(@$_SESSION['username']);
require 'database.php';
$safe_date = $mysqli->real_escape_string($date);
$safe_username = $mysqli->real_escape_string($username);

require 'database.php';
$stmt = $mysqli->prepare("SELECT content,time FROM events WHERE event_date=? and user_name=?");
 

	if( $stmt ){
	// Bind the parameter
		$stmt->bind_param('ss',$safe_date,$safe_username);
		$stmt->execute();
		// Bind the results
		$stmt->bind_result($event,$time);
		$i=0;
		$dataTime=array();
		$dataEvents=array();
		while($stmt->fetch()){
		    $dataTime[$i]=htmlentities($time);
			$dataEvents[$i]=htmlentities($event);
			$i+=1;
		};
		echo json_encode(array(
			"success" => true,
			"event" =>$dataEvents,
			"time" =>$dataTime
			
			));
		exit;
	}else{
			echo json_encode(array(
			"success" => false,
			"date" => $date,
			"message" => htmlentities("Save failed")
		));
		exit;
	}
?>