<?php
    //拿到session 并结束它
	header("Content-Type: application/json"); 
    ini_set("session.cookie_httponly", 1);
	session_start();
	$user = $_SESSION['username'];
	if($user){
    session_unset();	
    //session_destory();
	echo json_encode(array(
		"success" => true,
		"user" => htmlentities($user)
	));}
	else
	{
		echo json_encode(array(
		"success" => false,
		"user" => htmlentities($user)
	));
	}
?>


