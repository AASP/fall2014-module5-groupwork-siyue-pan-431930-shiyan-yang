<?php
	header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
	$username=htmlentities(@$_POST['username']);
	$password=htmlentities(@$_POST['password']);
	$password_crpt=crypt($password);

	//insert user and password into database
	require 'database.php';
	$stmt=$mysqli->prepare("insert into users (user_name,password) values (?,?)");	
	if( $stmt){
		session_start();
		$_SESSION['username'] = $username;
		$_SESSION['token'] = substr(md5(rand()), 0, 10);
		$stmt->bind_param('ss', $username, $password_crpt);
		$stmt->execute();
		$stmt->close();	
		echo json_encode(array(
		"success" => true,
		"user" => htmlentities($_SESSION['username']),
		"session" => htmlentities($_SESSION['token'])
		));
		exit;
	}else{
		
		echo json_encode(array(
		"success" => false,
		"message" => htmlentities("Incorrect Username or Password")
	));
	exit;
	}

?>