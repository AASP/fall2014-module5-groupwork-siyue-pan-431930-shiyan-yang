<?php
	header("Content-Type: application/json"); 	
	ini_set("session.cookie_httponly", 1);
	SESSION_START();
	$date = htmlentities(@$_POST['date']);
	//$event_time = $_POST['event_time'];
	$notes = htmlentities(@$_POST['notes']);
	$time = htmlentities(@$_POST['time']);
	$username =htmlentities(@$_SESSION['username']);
	
		require 'database.php';
        $stmt=$mysqli->prepare("insert into events (event_date,user_name,content,time) values (?,?,?,?)");
 
	if( $stmt ){
            $stmt->bind_param('ssss',$date,$username,$notes,$time);
            $stmt->execute();  
			
			echo json_encode(array(
			"success" => true,
			"date" => $date,
			"times" => $time,
			"notes" =>$notes,
			"username" =>$username
			));
			$stmt->close();
		exit;
	}else{
			echo json_encode(array(
			"success" => false,
			"message" => htmlentities("Save failed")
		));
		$stmt->close();
		exit;
	}

?>