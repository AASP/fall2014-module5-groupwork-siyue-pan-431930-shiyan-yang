<?php
	header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
	session_start();
	$date = htmlentities(@$_POST['date']);
	$time = htmlentities(@$_POST['time']);
	$notes = htmlentities(@$_POST['event']);
	$username = htmlentities(@$_POST['userShare']);
	// Use a prepared statement
	require 'database.php';
    $stmt=$mysqli->prepare("insert into events (event_date,user_name,content,time) values (?,?,?,?)");
 
	if( $stmt ){
            $stmt->bind_param('ssss',$date,$username,$notes,$time);
            $stmt->execute();  
			
			echo json_encode(array(
			"success" => true,
			"date" => $date,
			"times" => $time,
			"notes" =>$notes,
			"username" =>$username,
			"message" => "succeed"
			));
			$stmt->close();
		exit;
	}else{
			echo json_encode(array(
			"success" => false,
			"message" => htmlentities("Save failed")
		));
		$stmt->close();
		exit;
	}
 ?>